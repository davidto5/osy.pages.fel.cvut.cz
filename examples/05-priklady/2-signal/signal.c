#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <string.h>

int zalohuj = 0;

void handler(int num)
{
    zalohuj = 1;
}

int main()
{
    int prace = 40;
    struct sigaction action;
    memset(&action, 0, sizeof(action));
    action.sa_handler = handler;
    if (sigaction(SIGUSR1, &action, NULL) != 0) {
        return 2;
    }
    while (prace-- > 0) {
        printf("PRACUJI\n");
        sleep(1);
        if (zalohuj) {
            printf("Ukladam mezivysledek\n");
            zalohuj = 0;
        }
    }
    return 0;
}
