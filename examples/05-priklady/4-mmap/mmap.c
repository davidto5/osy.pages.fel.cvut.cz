#include <stdio.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <unistd.h>
#include <fcntl.h>
#include <err.h>

int main()
{
    char *shared_mem, buf[256];
    int fd = open("/tmp/mapedfile", O_RDWR | O_CREAT, 0600);
    printf("Open file %i\n", fd);
    for (int i = 0; i < 1000; i++) {
        sprintf(buf, "Data %03i\n", i);
        buf[9] = 0;
        write(fd, buf, 10);
    }
    shared_mem = mmap(NULL, 10000, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    if (shared_mem != MAP_FAILED) {
        printf("mmap %p\n", shared_mem);
        if (munmap(shared_mem, 10000) == -1) {
            err(1, "munmap");
        };
    }
    close(fd);

    return 0;
}
