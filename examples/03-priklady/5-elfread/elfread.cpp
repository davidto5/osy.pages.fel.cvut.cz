#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "elf.h"

int main(int argc, char *argv[])
{
    if (argc > 1) {
        int f = open(argv[1], O_RDONLY), i;
        unsigned char buf[2048];
        if (f >= 0) {
            read(f, buf, sizeof(Eh));
            Eh *eh = (Eh *)buf;
            printf("Magic %08x - %c%c%c%c\n", eh->ei_magic, eh->ei_magic & 0xff, (eh->ei_magic >> 8) & 0xff,
                   (eh->ei_magic >> 16) & 0xff, (eh->ei_magic >> 24) & 0xff);
            printf("Entry point %08lx\n", eh->entry);
            printf("Program headers Num=%i, offset=%lu size Eh %i\n", eh->ph_count, eh->ph_offset, sizeof(Ph));
            int num = eh->ph_count;
            int read_num = eh->ph_offset + num * 32;
            read(f, buf + sizeof(Eh), read_num - sizeof(Eh));
            for (i = 0; i < num; i++) {
                Ph *ph = (Ph *)&buf[eh->ph_offset + i * 32];
                printf("Program header n%i: type %i f_offs %08x, v_addr %08x, f_size %04x, flags %0x, align %i\n", i,
                       ph->type & 0xff, ph->f_offs, ph->v_addr, ph->f_size, ph->flags, ph->align);
            }
        }
    }
    return 0;
}
